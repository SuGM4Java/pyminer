import os

from PyQt5.QtWidgets import QWidget, QFileDialog,QMessageBox,QApplication
from typing import Tuple


def get_save_file_path(parent: 'QWidget' = None, caption: str = '',
                       directory: str = '', filter: str = '') -> str:
    file_path, file_type = QFileDialog.getSaveFileName(parent, caption, directory, filter)
    if os.path.exists(file_path):
        reply = QMessageBox.question(parent, '警告', '文件\n%s\n已存在，是否要替换？'%file_path, QMessageBox.Yes | QMessageBox.No, QMessageBox.Yes)
        print(reply==QMessageBox.Yes)
    return file_path

if __name__=='__main__':
    import sys
    from PyQt5.QtWidgets import  QApplication
    app=QApplication(sys.argv)
    get_save_file_path(None,'选择路径','','')
    app.exec_()