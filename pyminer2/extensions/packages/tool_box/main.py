import logging
logger = logging.getLogger(__name__)

from pyminer2.extensions.extensionlib import BaseExtension, BaseInterface
from .tool_box import ToolBox

class Extension(BaseExtension):

    def on_load(self):
        tool_box: 'ToolBox' = self.widgets['ToolBox']
        tool_box.extension_lib = self.extension_lib
        self.interface.file_widget = tool_box
        print("toolbox被加载")


class Interface(BaseInterface):
    pass
