<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="2.0" language="zh_CN" sourcelanguage="">
<context>
    <name>GeneralObjectViewer</name>
    <message>
        <location filename="../data_viewer.py" line="134"/>
        <source>value:</source>
        <translation>值：</translation>
    </message>
    <message>
        <location filename="../data_viewer.py" line="134"/>
        <source>meta data:</source>
        <translation>元数据：</translation>
    </message>
</context>
<context>
    <name>PMVariableTreeWidget</name>
    <message>
        <location filename="../main.py" line="128"/>
        <source>View</source>
        <translation>查看</translation>
    </message>
    <message>
        <location filename="../main.py" line="105"/>
        <source>Save as </source>
        <translation type="obsolete">另存为</translation>
    </message>
    <message>
        <location filename="../main.py" line="106"/>
        <source>Undo</source>
        <translation type="obsolete">撤销</translation>
    </message>
    <message>
        <location filename="../main.py" line="107"/>
        <source>Redo</source>
        <translation type="obsolete">重做</translation>
    </message>
    <message>
        <location filename="../main.py" line="132"/>
        <source>Delete</source>
        <translation>删除</translation>
    </message>
    <message>
        <location filename="../main.py" line="106"/>
        <source>Name</source>
        <translation>名称</translation>
    </message>
    <message>
        <location filename="../main.py" line="108"/>
        <source>Size</source>
        <translation>大小</translation>
    </message>
    <message>
        <location filename="../main.py" line="110"/>
        <source>Value</source>
        <translation>值</translation>
    </message>
    <message>
        <location filename="../main.py" line="133"/>
        <source>Open Variable</source>
        <translation>打开变量</translation>
    </message>
    <message>
        <location filename="../main.py" line="135"/>
        <source>Quick Save to Workdir</source>
        <translation>保存到工作路径</translation>
    </message>
    <message>
        <location filename="../main.py" line="137"/>
        <source>Save..</source>
        <translation>保存..</translation>
    </message>
    <message>
        <location filename="../main.py" line="141"/>
        <source>PyMiner Data(.pmd)</source>
        <translation>PyMiner数据(.pmd)</translation>
    </message>
    <message>
        <location filename="../main.py" line="142"/>
        <source>PyMiner Data(*.pmd)</source>
        <translation>PyMiner数据(*.pmd)</translation>
    </message>
    <message>
        <location filename="../main.py" line="143"/>
        <source>Pickle(.pkl)</source>
        <translation>Pickle(.pkl)</translation>
    </message>
    <message>
        <location filename="../main.py" line="144"/>
        <source>Pickle File(*.pkl)</source>
        <translation>Pickle文件(*.pkl)</translation>
    </message>
    <message>
        <location filename="../main.py" line="146"/>
        <source>Excel Table(.xls/.xlsx)</source>
        <translation>Excel表格(.xls/.xlsx)</translation>
    </message>
    <message>
        <location filename="../main.py" line="147"/>
        <source>Excel (*.xls *.xlsx)</source>
        <translation>Excel文件(*.xls *.xlsx)</translation>
    </message>
    <message>
        <location filename="../main.py" line="149"/>
        <source>Text File (.csv)</source>
        <translation>文本文件（.csv）</translation>
    </message>
    <message>
        <location filename="../main.py" line="150"/>
        <source>CSV File(*.csv)</source>
        <translation>CSV文件(*.csv)</translation>
    </message>
    <message>
        <location filename="../main.py" line="153"/>
        <source>Numpy Matrix</source>
        <translation>Numpy矩阵</translation>
    </message>
    <message>
        <location filename="../main.py" line="157"/>
        <source>Save Workspace</source>
        <translation>保存工作空间</translation>
    </message>
    <message>
        <location filename="../main.py" line="167"/>
        <source>Choose file</source>
        <translation type="obsolete">选择文件</translation>
    </message>
    <message>
        <location filename="../main.py" line="253"/>
        <source>Choose File</source>
        <translation>选择文件</translation>
    </message>
    <message>
        <location filename="../main.py" line="275"/>
        <source>Variable Name</source>
        <translation>变量名称</translation>
    </message>
    <message>
        <location filename="../main.py" line="275"/>
        <source>Please Input Variable Name:</source>
        <translation>请输入变量名:</translation>
    </message>
    <message>
        <location filename="../main.py" line="289"/>
        <source>Variable %s is corrupted.</source>
        <translation>变量 %s 损坏。</translation>
    </message>
    <message>
        <location filename="../main.py" line="291"/>
        <source>Cannot Load this type of file:%s</source>
        <translation>不支持加载此类文件：%s</translation>
    </message>
    <message>
        <location filename="../main.py" line="297"/>
        <source>Error Occurs In Loading</source>
        <translation>加载过程中出现错误</translation>
    </message>
    <message>
        <location filename="../main.py" line="339"/>
        <source>Choose File Name To Save</source>
        <translation>文件保存</translation>
    </message>
    <message>
        <location filename="../main.py" line="320"/>
        <source>File %s cannot be saved as its extension name is not supported</source>
        <translation>由于不支持此扩展名，文件%s无法保存</translation>
    </message>
</context>
<context>
    <name>PMWorkspaceInspectWidget</name>
    <message>
        <location filename="../main.py" line="259"/>
        <source>Expand All</source>
        <translation type="obsolete">展开全部</translation>
    </message>
    <message>
        <location filename="../main.py" line="260"/>
        <source>Collapse All</source>
        <translation type="obsolete">折叠全部</translation>
    </message>
    <message>
        <location filename="../main.py" line="524"/>
        <source>delete variable %s</source>
        <translation>删除变量%s</translation>
    </message>
    <message>
        <location filename="../main.py" line="488"/>
        <source>Variable Viewer</source>
        <translation type="unfinished">变量查看器</translation>
    </message>
</context>
<context>
    <name>app</name>
    <message>
        <location filename="../main.py" line="63"/>
        <source>Variable Viewer</source>
        <translation>变量查看器</translation>
    </message>
</context>
</TS>
