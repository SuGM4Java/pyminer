import os
import time
from typing import Dict, TYPE_CHECKING, Callable

from PyQt5.QtWidgets import QTreeWidget, QTreeWidgetItem, QWidget, QVBoxLayout, QMenu, \
    QMessageBox, QFileDialog, QInputDialog, QApplication
from PyQt5.QtCore import pyqtSignal, Qt, QLocale

import pandas as pd
import numpy as np
from pmgwidgets import PMDockObject, in_unit_test, create_translator
from pmtoolbox import load_variable_pmd, load_variable_pkl, save_variable_pkl, save_variable_pmd, save_variable_table, \
    save_variable_matrix

from pyminer2.extensions.extensionlib import BaseExtension, BaseInterface

if TYPE_CHECKING:
    from .data_viewer import PMVariableViewerWidget


class Extension(BaseExtension):
    interace: 'Interface' = None

    def on_loading(self):
        translation_file = os.path.join(os.path.dirname(__file__), 'translations',
                                        'qt_{0}.qm'.format(QLocale.system().name()))
        self.extension_lib.UI.add_translation_file(translation_file)
        self.extension_lib.Program.add_translation('zh_CN',
                                                   {'Workspace': '工作空间', 'Variable Viewer': '变量管理器',
                                                    'Collapse All': '全部折叠', 'Expand All': '全部展开'})

    def on_load(self):
        self.workspace: 'PMWorkspaceInspectWidget' = self.widgets['PMWorkspaceInspectWidget']
        self.workspace.var_tree.extension_lib = self.extension_lib
        self.workspace.extension_lib = self.extension_lib

        self.data_viewer: 'PMVariableViewerWidget' = None

        self.interface.data_viewer = self.workspace.var_tree
        self.workspace.bind_show_data(self.on_show_data)

        # self.workspace.connect_to_datamanager(self.extension_lib)
        # 注意：上边被注释掉的一句代码是既不规范也不正确的写法。因为当插件on_load方法调用的时候，
        # 尚未调用setup_ui方法，这就造成了一些相关的变量和控件尚未加载，可能获取不到。
        # 正确的做法如下所示
        self.extension_lib.Signal.get_widgets_ready_signal().connect(self.on_widgets_ready)

    def on_widgets_ready(self):
        self.workspace.set_extension_lib(self.extension_lib)
        self.extension_lib.UI.get_toolbar_widget('toolbar_home', 'button_open_variable').clicked.connect(
            self.workspace.var_tree.on_open_variable)
        self.extension_lib.UI.get_toolbar_widget('toolbar_home', 'button_save_workspace').clicked.connect(
            self.workspace.var_tree.on_save_workspace)
        self.extension_lib.get_interface('file_tree').add_open_file_callback('.pkl',
                                                                             self.workspace.var_tree.open_variable)
        self.extension_lib.get_interface('file_tree').add_open_file_callback('.pmd',
                                                                             self.workspace.var_tree.open_variable)

    def on_show_data(self, dataname: str):
        if self.data_viewer is None:
            app = QApplication.instance()
            self.data_viewer_cls = self.widget_classes['PMVariableViewerWidget']
            self.data_viewer: 'PMVariableViewerWidget' = \
                self.extension_lib.insert_widget(self.data_viewer_cls,
                                                 'new_dock_window',
                                                 {
                                                     "name": "data_view_table",
                                                     "side": "top",
                                                     "text": app.tr("Variable Viewer")})
            self.data_viewer.set_lib(self.extension_lib)
            self.data_viewer.on_dock_widget_deleted = self.on_dock_widget_deleted
        self.data_viewer.show_data(dataname)

    def on_dock_widget_deleted(self):
        self.data_viewer = None


class Interface(BaseInterface):
    def add_select_data_callback(self, callback: Callable):
        """
        添加数据被选中时的回调函数。也就是当你单击数据时候的回调函数。
        """
        self.data_viewer.add_select_data_callback(callback)


class PMVariableTreeWidget(QTreeWidget):
    signal_show_data_value = pyqtSignal(str)
    signal_data_saveas = pyqtSignal(str)
    signal_data_open = pyqtSignal(str)
    select_data_callbacks = []
    extension_lib: 'extension_lib' = None

    def __init__(self, parent=None):
        super().__init__(parent=None)
        self._show_data = lambda data_name: print('show data \'%s\'' % data_name)
        self.filter_all_upper_case = False
        self.filter_all_callables = True

    def is_item_legal(self, name: str, obj: object):
        return (not (callable(obj) and self.filter_all_callables)) and \
               (not (name.isupper() and self.filter_all_upper_case))

    def setup_ui(self):
        self.nodes: Dict[str, 'QTreeWidgetItem'] = {}
        self.setColumnCount(3)
        header_item = QTreeWidgetItem()
        header_item.setText(0, self.tr('Name'))
        self.setColumnWidth(0, 10 * 10)
        header_item.setText(1, self.tr('Size'))
        self.setColumnWidth(1, 10 * 10)
        header_item.setText(2, self.tr('Value'))
        header_item.setTextAlignment(0, Qt.AlignCenter)
        header_item.setTextAlignment(1, Qt.AlignCenter)
        header_item.setTextAlignment(2, Qt.AlignCenter)
        self.setHeaderItem(header_item)
        self.auto_expand = False

        self.itemClicked.connect(self.on_item_clicked)
        self.itemDoubleClicked.connect(self.on_item_double_clicked)
        self.customContextMenuRequested.connect(self.on_right_clicked)
        self.setContextMenuPolicy(Qt.CustomContextMenu)

    def create_context_menu(self) -> QMenu:
        """
        创建右键菜单。由于右键菜单用的不会太多，所以动态生成应当也是可以接受的。
        :return:
        """
        context_menu = QMenu()
        show_action = context_menu.addAction(self.tr('View'))
        # save_action = self.context_menu.addAction(self.tr('Save as '))
        # cancel_action = self.context_menu.addAction(self.tr('Undo'))
        # redo_action = self.context_menu.addAction(self.tr('Redo'))
        delete_action = context_menu.addAction(self.tr('Delete'))
        open_action = context_menu.addAction(self.tr('Open Variable'))

        save_to_workdir = context_menu.addAction(self.tr('Quick Save to Workdir'))

        save_menu = QMenu(parent=context_menu, title=self.tr('Save..'))
        context_menu.addMenu(save_menu)
        var = self.get_current_var()
        # save_var_action = save_menu.addAction()
        save_to_pmd_action = save_menu.addAction(self.tr('PyMiner Data(.pmd)'))
        save_to_pmd_action.triggered.connect(lambda: self.on_save_variable(self.tr('PyMiner Data(*.pmd)')))
        save_to_pkl_action = save_menu.addAction(self.tr('Pickle(.pkl)'))
        save_to_pkl_action.triggered.connect(lambda: self.on_save_variable(self.tr('Pickle File(*.pkl)')))
        if isinstance(var, pd.DataFrame):
            save_to_excel_action = save_menu.addAction(self.tr('Excel Table(.xls/.xlsx)'))
            save_to_excel_action.triggered.connect(
                lambda: self.on_save_to_table(self.tr("Excel (*.xls *.xlsx)")))
            save_to_csv_action = save_menu.addAction(self.tr('Text File (.csv)'))
            save_to_csv_action.triggered.connect(
                lambda: self.on_save_to_table(self.tr("CSV File(*.csv)")))
        if isinstance(var, np.ndarray):
            save_to_npy_action = save_menu.addAction(self.tr('Numpy Matrix'))
            save_to_npy_action.triggered.connect(self.on_save_to_npy)
        # action_save_as_table = save_menu.addAction('aaaaaa')

        save_workspace_action = context_menu.addAction(self.tr('Save Workspace'))
        show_action.triggered.connect(self.on_show_data_by_context_menu)
        delete_action.triggered.connect(self.on_delete_data_by_context_menu)
        save_to_workdir.triggered.connect(self.on_save_to_workdir)
        open_action.triggered.connect(self.on_open_variable)

        save_workspace_action.triggered.connect(self.on_save_workspace)
        return context_menu

    def on_save_to_npy(self):
        file_path, filetype = QFileDialog.getSaveFileName(self,
                                                          self.tr('Choose File'), self._get_work_dir(),
                                                          "Numpy Matrix(*.npy)")  # 设置文件扩展名过滤,用双分号间隔
        save_variable_matrix(self.get_current_var(), file_path)

    def on_save_to_table(self, ext_type: str):
        file_path, filetype = QFileDialog.getSaveFileName(self,
                                                          self.tr('Choose File'), self._get_work_dir(),
                                                          self.tr(ext_type))
        save_variable_table(self.get_current_var(), file_path)

    def on_item_clicked(self, item: QTreeWidgetItem, col: int) -> None:
        """
        点击条目时的回调函数。会顺次执行self.select_data_callbacks，而self.select_data_callbacks
        是由插件的add_select_data_callback(self, callback: Callable)方式加入的。
        """
        if item.isExpanded():
            self.collapseItem(item)
        else:
            self.expandItem(item)

        if item.text(0) in self.nodes.keys():
            for callback in self.select_data_callbacks:
                callback(item.text(0))

    def on_item_double_clicked(self, item: QTreeWidgetItem, col: int) -> None:
        """
        双击条目的事件，触发数据显示。
        """
        if col == 0 and item.text(0) in self.nodes.keys():
            self.show_data(item.text(0))

    def on_right_clicked(self, pos):
        """
        右键点击某一条目的槽,此时会弹出菜单。
        """
        item = self.currentItem()
        if item is not None and item.text(0) in self.nodes.keys():
            context_menu = self.create_context_menu()
            context_menu.exec_(self.mapToGlobal(pos))

    def on_delete_data_by_context_menu(self):
        item = self.currentItem()
        if item is not None and item.text(0) in self.nodes.keys():
            self.delete_data(item.text(0))

    def get_current_var_name(self) -> str:
        """
        获取当前选中变量的名称
        :return:
        """
        item = self.currentItem()
        if item is not None and item.text(0) in self.nodes.keys():
            return item.text(0)
        return ''

    def _get_work_dir(self) -> str:
        if not in_unit_test():
            work_dir = self.extension_lib.Program.get_work_dir()
        else:
            work_dir = os.path.join(os.path.expanduser('~'), "Desktop")
        return work_dir

    def get_current_var(self):
        if not in_unit_test():
            var = self.extension_lib.Data.get_var(self.get_current_var_name())
        else:
            global data_dic
            var = data_dic.get(self.get_current_var_name())
        return var

    def on_save_to_workdir(self):
        var_name = self.get_current_var_name()
        if var_name != '':
            work_dir = self._get_work_dir()
            import cloudpickle
            file_dir = os.path.join(work_dir, var_name + '.pkl')
            try:
                with open(file_dir, 'wb') as f:
                    cloudpickle.dump(self.get_current_var(), f)

            except:
                import traceback
                traceback.print_exc()

    def on_open_variable(self):
        file_path, filetype = QFileDialog.getOpenFileName(self,
                                                          self.tr('Choose File'), self._get_work_dir(),
                                                          "Data File(*.pkl *.pmd)")  # 设置文件扩展名过滤,用双分号间隔
        self.open_variable(file_path)

    def open_variable(self, file_path: str):
        if os.path.exists(file_path) and file_path != '':
            try:
                if file_path.endswith('.pmd'):
                    variables, metadatas = load_variable_pmd(file_path)
                    if variables is not None:
                        if not in_unit_test():
                            self.extension_lib.Data.update_var_dic(variables, provider='workspace')
                            # TODO:(侯展意)目前还无法将metadata读入。这个或许需要问一下。
                        else:
                            print(variables, metadatas)
                        return
                    else:
                        error_message = self.tr('Variable %s is corrupted.' % file_path)
                elif file_path.endswith('.pkl'):
                    base_name, ext = os.path.splitext(os.path.basename(file_path))
                    while 1:
                        var_name, ok = QInputDialog.getText(self, self.tr('Variable Name'),
                                                            self.tr('Please Input Variable Name:'), text=base_name)
                        if not ok:
                            return
                        if var_name.isidentifier():
                            break
                    variable = load_variable_pkl(file_path)
                    if variable is not None:
                        if not in_unit_test():
                            self.extension_lib.Data.set_var(var_name, variable)
                        else:
                            print(var_name, variable)
                        return
                    else:
                        error_message = self.tr('Variable %s is corrupted.' % file_path)
                else:
                    error_message = self.tr('Cannot Load this type of file:%s' % file_path)

            except Exception as e:
                import traceback
                traceback.print_exc()
                error_message = e
            QMessageBox.warning(self, self.tr('Error Occurs In Loading'), error_message)

    def on_save_variable(self, ext_type: str):
        work_dir = self._get_work_dir()
        if not in_unit_test():

            var_name = self.get_current_var_name()
            value = self.extension_lib.Data.get_var(var_name)
            metadata = self.extension_lib.Data.get_metadata(var_name)
        else:
            var_name = 'abc'
            value = 123
            metadata = {'time': time.time()}
        export_path, ext = QFileDialog.getSaveFileName(self, self.tr("Choose File Name To Save"),
                                                       os.path.join(work_dir, var_name),
                                                       ext_type)  # ;;Numpy数组文件(*.npy)")
        if export_path != '':
            if export_path.endswith('.pkl'):
                save_variable_pkl(value, export_path)
            elif export_path.endswith('.pmd'):
                print(export_path)
                save_variable_pmd(var_name, value, export_path, metadata)
            else:
                raise ValueError(self.tr('File %s cannot be saved as its'
                                         ' extension name is not supported') % export_path)

    def on_save_workspace(self):
        work_dir = self._get_work_dir()
        if not in_unit_test():

            variables = self.extension_lib.Data.get_all_public_variables()
            metadata = self.extension_lib.Data.get_all_metadata()
        else:
            variables = {'a': 123, 'b': '123123123'}
            metadata = {'a': {'time': time.time()}, 'b': {'time': time.time()}}
        var_name_list = []
        var_value_list = []
        var_metadata_list = []
        for k, v in variables.items():
            var_name_list.append(k)
            var_value_list.append(v)
            var_metadata_list.append(metadata.get(k) if k in metadata else {})
        export_path, ext = QFileDialog.getSaveFileName(self, self.tr("Choose File Name To Save"), work_dir,
                                                       r"PyMiner Data File(*.pmd)")  # ;;Numpy数组文件(*.npy)")
        if export_path != '':
            save_variable_pmd(var_name_list, var_value_list, export_path, var_metadata_list)

    def add_select_data_callback(self, callback: Callable):
        """
        加入回调函数。
        """

        self.select_data_callbacks.append(callback)

    def on_show_data_by_context_menu(self, action: 'QAction'):
        """
        右键菜单点击‘显示数据’所触发的事件。
        """
        item = self.currentItem()
        if item is not None and item.text(0) in self.nodes.keys():
            print(item.text(0))
            self.show_data(item.text(0))

    def autorepr(self, obj: object, max_length=80) -> str:
        """
        封装了repr方法，数据过长的时候，取较短的。
        """
        return repr(obj).replace('\n', '').replace('\r', '')[:max_length]

    def get_size(self, data: object):
        """
        获取数据的尺寸。
        有‘shape’这一属性的，size就等于data.shape
        有‘__len__’这个属性的，返回len(data)
        否则返回1.
        """
        size = 1
        if hasattr(data, 'shape'):
            size = data.shape
        elif hasattr(data, '__len__'):
            try:
                size = len(data)
            except:
                pass
        return size

    def delete_data(self, data_name: str) -> None:
        """
        删除数据的回调
        :param data_name:
        :return:
        """
        if not in_unit_test():
            self.extension_lib.Data.delete_variable(data_name)
        else:
            print('in unit test.delete data:%s' % data_name)

    def clear_all_nodes(self):
        self.nodes = {}
        self.clear()

    def set_data(self, data_dic: Dict[str, object]) -> None:
        """
        显示数据
        :param data_dic:所有数据的字典{变量名：变量对象}
        :return:
        """
        for data_name in data_dic:
            data = data_dic[data_name]
            if not self.is_item_legal(data_name, data):
                continue
            size = self.get_size(data)
            data_str = f'<{size}>\t{self.autorepr(data)}'

            if data_name in self.nodes.keys():
                """
                如果变量在变量列表中，就刷新这个变量位置显示的文字信息，并通过递归方式创建变量的各个属性。
                """
                data_node = self.nodes[data_name]
                data_node.setText(0, data_name)
                data_node.setText(1, repr(size))
                data_node.setText(2, self.autorepr(data))
                data_node.takeChildren()

                # if isinstance(data, dict):
                #     self.set_data_view(data, self.nodes[data_name])
            else:
                """
                如果变量不在变量列表中，就新建一个节点，并通过递归方式创建变量的各个属性。
                """
                child = QTreeWidgetItem(self)
                self.nodes[data_name] = child
                child.setText(0, data_name)
                child.setText(1, str(size))
                child.setText(2, self.autorepr(data))

                # if isinstance(data, dict):
                #     self.set_data_view(data, child)
                if self.auto_expand:
                    self.expandItem(child)
                else:
                    self.collapseItem(child)

    def get_hier(self, item: QTreeWidgetItem) -> int:
        i = 0
        all_nodes = [self.nodes[k] for k in self.nodes.keys()]
        while (1):
            if item.parent() not in all_nodes:
                i += 1
                item = item.parent()
            else:
                return i

    def set_data_view(self, data_dic: Dict[str, object], root: 'QTreeWidgetItem'):
        """
        递归方式显示json。
        显示的时候要注意层级。
        :param data_dic:
        :param root:
        :return:
        """
        for k in data_dic.keys():
            if type(data_dic[k]) == dict:

                child = QTreeWidgetItem(root)
                # self.indexOfTopLevelItem(child)
                child.setText(0, repr(k))
                if self.get_hier(child) <= 1:
                    self.set_data_view(data_dic[k], child)
                # print(self.get_hier(child))
            else:
                child = QTreeWidgetItem(root)
                child.setText(0, repr(k))
                child.setText(1, str(self.get_size(data_dic[k])))
                child.setText(2, self.autorepr(data_dic[k]))

    def show_data(self, data_name):
        return self._show_data(data_name)


class PMWorkspaceInspectWidget(QWidget, PMDockObject):
    """
    用于承载 PMVariableTreeWidget以及相关的按钮。

    """

    def __init__(self, parent=None):
        super().__init__(parent)
        self.translator = create_translator(
            path=os.path.join(os.path.dirname(__file__), 'translations',
                              'qt_{0}.qm'.format(QLocale.system().name())))
        # print(self.tr('Variable Viewer'))
        layout = QVBoxLayout()
        self.var_tree = PMVariableTreeWidget(parent)
        layout.addWidget(self.var_tree)
        self.setLayout(layout)

    def setup_ui(self):
        self.var_tree.setup_ui()
        layout = self.layout()
        layout.setContentsMargins(0, 0, 0, 0)

        self.signal_show_data_value = self.var_tree.signal_show_data_value
        self.signal_data_saveas = self.var_tree.signal_data_saveas
        self.signal_data_open = self.var_tree.signal_data_open

    def set_extension_lib(self, extension_lib):
        """
        与数据管理相连接。
        同时，在这个函数中定义了闭包子函数on_modification和on_deletion，
        也就是在数据变更或者删除时候调用的方法。
        """
        self.extension_lib = extension_lib
        self.data = self.extension_lib.Data.get_all_variables()
        self.var_tree.set_data(self.extension_lib.Data.get_all_public_variables())

        def on_changed(varname: str, variable, data_source: str):
            self.data[varname] = variable
            self.var_tree.set_data({varname: variable})
            # need to detect whether it is modified or created

        def on_deletion(varname: str, provider: str):
            self.extension_lib = extension_lib
            self.data = self.extension_lib.Data.get_all_variables()
            self.var_tree.clear_all_nodes()
            self.var_tree.set_data(self.extension_lib.Data.get_all_public_variables())
            if provider != 'ipython':
                self.extension_lib.get_interface('ipython_console').run_command(
                    '__delete_var(\'%s\')' % varname,
                    hint_text=self.tr('delete variable %s') % varname,
                    hidden=False)

        self.extension_lib.Data.add_data_changed_callback(on_changed)
        self.extension_lib.Data.add_data_deleted_callback(on_deletion)

    def bind_show_data(self, on_show_data):
        """
        绑定变量树视图在show_data事件触发时的回调。
        """
        self.var_tree.show_data = on_show_data


if __name__ == '__main__':
    from PyQt5.QtWidgets import QTableWidget
    import sys

    app = QApplication(sys.argv)
    sa = PMWorkspaceInspectWidget()
    sa.show()
    sa.setup_ui()
    data_dic = {'a': {'type': 'statespace',
                      'A': {'type': 'timeseries', 'time': '[1,2,3]', 'mdata': '[3,2,1]'},
                      'B': {'type': 'matrix', 'value': '[[2],[1]]', 'aaaa': {'a': 'aa', 0: {'a': 'a'}}},
                      'C': {'type': 'matrix', 'value': '[[1,2]]', },
                      'D': {'type': 'matrix', 'value': '[[0]]', },
                      'row': ['left', 'x2'], 'column': ['column'], 'u': ['u'], 'sys': 'str'},
                'b': 100,
                'c': np.array([1, 2, 3, 4, 5]),
                'd': pd.DataFrame([[1, 2, 3], [4, 5, 6]])}
    sa.var_tree.set_data(data_dic)
    sys.exit(app.exec())
