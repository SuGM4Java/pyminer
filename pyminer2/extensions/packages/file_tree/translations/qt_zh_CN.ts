<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="2.0" language="zh_CN" sourcelanguage="">
<context>
    <name>PMFilesTree</name>
    <message>
        <location filename="../file_tree.py" line="39"/>
        <source>Back</source>
        <translation type="unfinished">后退</translation>
    </message>
    <message>
        <location filename="../file_tree.py" line="71"/>
        <source>Open Path</source>
        <translation type="unfinished">打开路径</translation>
    </message>
    <message>
        <location filename="../file_tree.py" line="48"/>
        <source>Parent Path</source>
        <translation type="unfinished">父路径</translation>
    </message>
</context>
<context>
    <name>PMFilesTreeview</name>
    <message>
        <location filename="../file_tree.py" line="79"/>
        <source>Open</source>
        <translation type="obsolete">打开</translation>
    </message>
    <message>
        <location filename="../file_tree.py" line="80"/>
        <source>Import</source>
        <translation type="obsolete">导入</translation>
    </message>
    <message>
        <location filename="../file_tree.py" line="81"/>
        <source>Rename</source>
        <translation type="obsolete">重命名</translation>
    </message>
    <message>
        <location filename="../file_tree.py" line="82"/>
        <source>Delete</source>
        <translation type="obsolete">删除</translation>
    </message>
</context>
<context>
    <name>RewriteQFileSystemModel</name>
    <message>
        <location filename="../file_tree.py" line="17"/>
        <source>Name</source>
        <translation type="obsolete">名称</translation>
    </message>
    <message>
        <location filename="../file_tree.py" line="19"/>
        <source>Size</source>
        <translation type="obsolete">大小</translation>
    </message>
    <message>
        <location filename="../file_tree.py" line="21"/>
        <source>Type</source>
        <translation type="obsolete">类型</translation>
    </message>
    <message>
        <location filename="../file_tree.py" line="23"/>
        <source>Last Modified</source>
        <translation type="obsolete">修改日期</translation>
    </message>
</context>
</TS>
