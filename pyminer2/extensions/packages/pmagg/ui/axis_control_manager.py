"""
负责对生成的ui类绑定事件，添加交互逻辑
"""
import os
from .axis_control import Ui_Dialog
from PyQt5 import QtWidgets, QtGui
import configparser
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.ticker import LogLocator, MaxNLocator, MultipleLocator, LinearLocator, ScalarFormatter
from matplotlib.axes import Axes


class Ui_Dialog_Manager(Ui_Dialog):
    def __init__(self, ax, canvas: FigureCanvas, config: configparser.ConfigParser, path):
        self.ax = ax
        self.canvas = canvas
        self.config = config
        self.current_path = path
        self.dialog = QtWidgets.QDialog()
        self.setupUi(self.dialog)
        self.retranslateUi(self.dialog)
        self.treeWidget.setColumnCount(1)
        self.treeWidget.setHeaderLabels(['子图'])
        self.axes = self.canvas.figure.get_axes()
        for index, item in enumerate(range(len(self.axes))):
            root = QtWidgets.QTreeWidgetItem(self.treeWidget)
            root.setText(0, '子图' + str(index + 1))
            root.setIcon(0, QtGui.QIcon(os.path.join(self.current_path, 'icons/figure.png')))
            self.treeWidget.addTopLevelItem(root)
            ax = self.axes[index]
            if hasattr(ax, 'xaxis'):
                child = QtWidgets.QTreeWidgetItem()
                child.setText(0, 'X轴')
                child.setIcon(0, QtGui.QIcon(os.path.join(self.current_path, 'icons/X_axis.png')))
                root.addChild(child)
            if hasattr(ax, 'yaxis'):
                child = QtWidgets.QTreeWidgetItem()
                child.setText(0, 'Y轴')
                child.setIcon(0, QtGui.QIcon(os.path.join(self.current_path, 'icons/Y_axis.png')))
                root.addChild(child)
            if hasattr(ax, 'zaxis'):
                child = QtWidgets.QTreeWidgetItem()
                child.setText(0, 'Z轴')
                child.setIcon(0, QtGui.QIcon(os.path.join(self.current_path, 'icons/Z_axis.png')))
                root.addChild(child)
        self.treeWidget.clicked.connect(self.on_clicked)
        self.pushButton_3.clicked.connect(self.apply_slot)
        self.pushButton_2.clicked.connect(self.cancel_slot)
        self.pushButton.clicked.connect(self.confirm_slot)
        self.btn_group = QtWidgets.QButtonGroup(self.dialog)
        self.btn_group.addButton(self.radioButton)
        self.btn_group.addButton(self.radioButton_2)
        self.btn_group.addButton(self.radioButton_3)
        self.btn_group.addButton(self.radioButton_4)
        self.current_subplot = None
        self.current_ax = None
        self.current_ax_attribute = None  # 判断当前是X轴还是Y轴，Z轴
        self.axes_is_AxesSubplot = None
        self.scales = ['linear', 'log', 'symlog', 'logit']
        self.x_ticks_positions = ['top', 'bottom']
        self.y_ticks_positions = ['left', 'right']
        self.ticks_directions = ['in', 'out', 'inout']
        self.dialog.exec_()  # 初始化之后再执行

    def on_clicked(self):
        self.item = self.treeWidget.currentItem()
        ax_min = ax_max = ''
        if not self.axes:
            return
        # 清除所有值
        self.lineEdit_3.setText('')
        self.lineEdit_4.setText('')
        self.lineEdit_7.setText('')
        self.lineEdit_8.setText('')
        self.comboBox_2.clear()
        self.checkBox_7.setChecked(False)
        if self.item.parent() is None:
            self.current_subplot = self.axes[int(self.item.text(0)[-1]) - 1]
            self.current_ax = self.current_subplot.xaxis
            ax_min, ax_max = self.current_subplot.get_xlim()
            self.comboBox_2.addItems(self.ticks_directions)
            self.comboBox_2.setCurrentText('out')
            self.current_ax_attribute = 'X'
        else:
            self.current_subplot = self.axes[int(self.item.parent().text(0)[-1]) - 1]
        if self.item.text(0)[0] == 'X':
            self.current_ax = self.current_subplot.xaxis
            ax_min, ax_max = self.current_subplot.get_xlim()
            self.comboBox_2.addItems(self.ticks_directions)
            self.comboBox_2.setCurrentText('out')
            self.lineEdit_5.setText(self.current_subplot.get_xlabel())
            self.current_ax_attribute = 'X'
        if self.item.text(0)[0] == 'Y':
            self.current_ax = self.current_subplot.yaxis
            ax_min, ax_max = self.current_subplot.get_ylim()
            self.comboBox_2.addItems(self.ticks_directions)
            self.comboBox_2.setCurrentText('out')
            self.current_ax_attribute = 'Y'
        if self.item.text(0)[0] == 'Z':
            self.current_ax = self.current_subplot.zaxis
            ax_min, ax_max = self.current_subplot.get_zlim()
            self.current_ax_attribute = 'Z'
        self.axes_is_AxesSubplot = self.current_subplot.__class__.__name__ == 'AxesSubplot'  # 该类是工厂生成的类，不会存在模块级别
        for item in [self.checkBox, self.checkBox_2, self.checkBox_3, self.checkBox_4, self.checkBox_5,
                     self.checkBox_6,self.lineEdit_8,self.lineEdit_7]:
            item.setEnabled(self.axes_is_AxesSubplot)
        self.radio_scale()
        self.lineEdit.setText(str(ax_min))
        self.lineEdit_2.setText(str(ax_max))
        self.checkBox_6.setChecked(True)
        self.checkBox_2.setChecked(True)
        self.checkBox.setChecked(True)
        self.checkBox_3.setChecked(True)
        self.checkBox_4.setChecked(False)
        self.checkBox_5.setChecked(False)
        self.lineEdit_5.setText(self.current_ax.get_label_text())

    def radio_scale(self):
        """坐标轴类型选择"""
        if self.current_ax is None:
            return
        scale = self.current_ax.get_scale()
        if scale == 'linear':
            self.radioButton.setChecked(True)
            self.lineEdit_3.setEnabled(True)
            self.lineEdit_4.setEnabled(True)
        if scale == 'log':
            self.radioButton_2.setChecked(True)
            self.lineEdit_3.setEnabled(False)
            self.lineEdit_4.setEnabled(False)
        if scale == 'symlog':
            self.radioButton_3.setChecked(True)
            self.lineEdit_3.setEnabled(False)
            self.lineEdit_4.setEnabled(False)
        if scale == 'logit':
            self.radioButton_3.setChecked(True)
            self.lineEdit_3.setEnabled(False)
            self.lineEdit_4.setEnabled(False)

    @staticmethod
    def is_number(s):
        try:
            float(s)
            return True
        except ValueError:
            return False

    def apply_slot(self):
        if not len(self.axes) or not self.current_subplot or not self.current_ax:
            return
        if self.current_ax_attribute == 'X':
            self.current_subplot.set_xscale(self.scales[abs(self.btn_group.checkedId()) - 2])
            self.current_subplot.set_xlim(float(self.lineEdit.text()), float(self.lineEdit_2.text()))
            if self.checkBox_7.isChecked():
                self.current_subplot.invert_xaxis()
            if self.axes_is_AxesSubplot:
                self.current_subplot.spines['bottom'].set_visible(self.checkBox_6.isChecked())
                self.current_subplot.spines['top'].set_visible(self.checkBox_3.isChecked())
                if self.is_number(self.lineEdit_7.text()):  # 设置坐标轴位置
                    self.current_subplot.spines['bottom'].set_position(('data', float(self.lineEdit_7.text())))
                self.current_subplot.tick_params(axis="x",
                                                 which='both',
                                                 direction=self.comboBox_2.currentText(),
                                                 bottom=self.checkBox_2.isChecked(),
                                                 top=self.checkBox_4.isChecked(),
                                                 labelbottom=self.checkBox.isChecked(),
                                                 labeltop=self.checkBox_5.isChecked())
                if self.is_number(self.lineEdit_8.text()):
                    self.current_subplot.set_xticklabels(self.current_subplot.get_xticklabels(),
                                                         rotation=float(self.lineEdit_8.text()))
        if self.current_ax_attribute == 'Y':
            self.current_subplot.set_yscale(self.scales[abs(self.btn_group.checkedId()) - 2])
            self.current_subplot.set_ylim(float(self.lineEdit.text()), float(self.lineEdit_2.text()))
            if self.checkBox_7.isChecked():
                self.current_subplot.invert_yaxis()
            if self.axes_is_AxesSubplot:
                self.current_subplot.spines['left'].set_visible(self.checkBox_6.isChecked())
                self.current_subplot.spines['right'].set_visible(self.checkBox_3.isChecked())
                if self.is_number(self.lineEdit_7.text()):  # 设置坐标轴位置
                    self.current_subplot.spines['left'].set_position(('data', float(self.lineEdit_7.text())))
                self.current_subplot.tick_params(axis="y",
                                                 which='both',
                                                 direction=self.comboBox_2.currentText(),
                                                 left=self.checkBox_2.isChecked(),
                                                 right=self.checkBox_4.isChecked(),
                                                 labelleft=self.checkBox.isChecked(),
                                                 labelright=self.checkBox_5.isChecked())
            if self.is_number(self.lineEdit_8.text()):
                self.current_subplot.set_yticklabels(self.current_subplot.get_yticklabels(),
                                                     rotation=float(self.lineEdit_8.text()))
        if self.current_ax_attribute == 'Z':
            self.current_subplot.set_zscale(self.scales[abs(self.btn_group.checkedId()) - 2])
            self.current_subplot.set_zlim(float(self.lineEdit.text()), float(self.lineEdit_2.text()))
            if self.checkBox_7.isChecked():
                self.current_subplot.invert_zaxis()
        if self.radioButton.isChecked():
            if self.is_number(self.lineEdit_3.text()):
                major_tick_spacing = float(self.lineEdit_3.text())
                self.current_ax.set_major_locator(MultipleLocator(major_tick_spacing))
            if self.is_number(self.lineEdit_4.text()):
                minor_tick_spacing = float(self.lineEdit_4.text())
                self.current_ax.set_minor_locator(MultipleLocator(minor_tick_spacing))
        self.current_ax.set_label_text(self.lineEdit_5.text())
        self.canvas.draw()

    def cancel_slot(self):
        self.dialog.close()

    def confirm_slot(self):
        self.apply_slot()
        self.dialog.close()
