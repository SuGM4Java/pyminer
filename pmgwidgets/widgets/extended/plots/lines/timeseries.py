from typing import List, Dict, Tuple
from PyQt5.QtWidgets import QHBoxLayout
from pmgwidgets.widgets.extended.base import BaseExtendedWidget
from pmgwidgets import iter_isinstance, TYPE_RANGE


class PMGTimeSeriesShow(BaseExtendedWidget):
    def __init__(self, layout_dir: str, title: str, initial_value: Dict[str, List[float]],
                 y_range: Tuple[int, int] = None, xlabel: str = '', ylabel: str = '',
                 threshold_range: TYPE_RANGE = None):
        super().__init__(layout_dir=layout_dir)
        from pmgwidgets import PMGTimeSeriesPlot
        entryLayout = QHBoxLayout()
        entryLayout.setContentsMargins(0, 0, 0, 0)

        self.ctrl = PMGTimeSeriesPlot(parent=None, threshold_range=threshold_range)
        self.ctrl.time_series.xlabel = xlabel
        self.ctrl.time_series.ylabel = ylabel
        self.ctrl.time_series.title = title
        self.ctrl.time_series.y_range = y_range
        self.ctrl.time_series.threshold_range = threshold_range
        self.central_layout.addLayout(entryLayout)
        entryLayout.addWidget(self.ctrl)

        self.accury = initial_value
        self.set_value(initial_value)

    def set_value(self, values: Dict[str, List[float]]):
        """
        {'timestamps':[...],
         'info1':{name:'unnamed','data':[...]}
         }
        :param values:
        :return:
        """
        timestamps = values['timestamps']
        values_list = []
        tags_list = []
        length = len(timestamps)
        for k in values:
            if k != 'timestamps':
                assert len(values[k]['data']) == length,'timestamps and datas may not be of same length!'
                assert values[k].get('data') is not None
                assert values[k].get('tag') is not None
                values_list.append(values[k]['data'])
                tags_list.append(values[k]['tag'])
        self.ctrl.set_data(timestamps, values=values_list, tags=tags_list)

    def alert(self, alert_level: int):
        self.ctrl.alert(alert_level)

    def set_threshold_y(self, threshold_y: float):
        self.ctrl.time_series.threshold_line = threshold_y
